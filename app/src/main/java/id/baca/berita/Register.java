package id.baca.berita;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.ArrayList;

import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.model.ModelMember;

public class Register extends AppCompatActivity {

    ImageButton back;
    RelativeLayout a,biru;
    Button butregis;
    EditText edusername,edemail;
    ShowHidePasswordEditText edpass;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    MCrypt mCrypt;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z.]+";
        mCrypt = new MCrypt();

        back = findViewById(R.id.back);
        a = findViewById(R.id.a);
        biru = findViewById(R.id.biru);
        butregis = findViewById(R.id.butregis);
        edusername = findViewById(R.id.edusername);
        edemail = findViewById(R.id.edemail);
        edpass = findViewById(R.id.edpass);

        a.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide();
                return false;
            }
        });

        biru.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide();
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();
                onBackPressed();
            }
        });

        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        butregis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edusername.getText().toString().trim().equals("")||edusername.length()==0){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("User Name Still Empty");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (edemail.getText().toString().trim().equals("")||edemail.length()==0){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Email Still Empty");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (!edemail.getText().toString().trim().matches(emailPattern)){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Invalid Email");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (edpass.getText().toString().trim().equals("")||edpass.length()==0){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Password Still Empty");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (edpass.length()<7){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Password must be 7 characters");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else{
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = getLayoutInflater().inflate(R.layout.dialog_konfirm, null);
                    TextView btnok = view.findViewById(R.id.btnok);
                    TextView btnno = view.findViewById(R.id.btnno);
                    TextView txkeluar = view.findViewById(R.id.txkeluar);
                    TextView txkeluar1 = view.findViewById(R.id.txkeluar1);
                    mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();


                    txkeluar.setText("Confirmation");
                    txkeluar1.setText("Are you sure the data you entered is correct?");
                    btnok.setText("Yes");
                    btnno.setText("No");


                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mBottomSheetDialog.dismiss();
                            pDialog = new ProgressDialog(Register.this);
                            pDialog.setCancelable(false);
                            pDialog.show();
                            pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                            pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            pDialog.setContentView(R.layout.layout_loader);
                            mFirebaseAuth = FirebaseAuth.getInstance();
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();

                            mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                                    .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                                cekduplikasi();


                                            } else {
                                                final Dialog mBottomSheetDialog;
                                                final View view;
                                                view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                                                TextView judul = view.findViewById(R.id.judul);
                                                TextView teks2 = view.findViewById(R.id.pesanDialog);
                                                final Button btnok = view.findViewById(R.id.Ya);
                                                ImageView gambar = view.findViewById(R.id.gambar);
                                                mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                                                mBottomSheetDialog.setContentView(view);
                                                mBottomSheetDialog.setCancelable(false);
                                                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                                mBottomSheetDialog.show();

                                                judul.setText("Failed");
                                                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                                btnok.setText("Ok");

                                                teks2.setText("Failed to load data, please try again");

                                                btnok.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mBottomSheetDialog.dismiss();



                                                    }
                                                });

                                                btnok.setOnTouchListener(new View.OnTouchListener() {
                                                    @Override
                                                    public boolean onTouch(View v, MotionEvent event) {
                                                        switch (event.getAction()) {
                                                            case MotionEvent.ACTION_DOWN: {
                                                                Button view = (Button) v;
                                                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                                view.invalidate();
                                                                break;
                                                            }
                                                            case MotionEvent.ACTION_UP:
                                                            case MotionEvent.ACTION_CANCEL: {
                                                                Button view = (Button) v;
                                                                view.getBackground().clearColorFilter();
                                                                view.invalidate();
                                                                break;
                                                            }
                                                        }

                                                        return false;
                                                    }
                                                });
                                                pDialog.dismiss();
                                            }
                                        }
                                    });
                        }
                    });


                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    TextView view = (TextView) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    TextView view = (TextView) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                    btnno.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();
                        }
                    });

                    btnno.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    TextView view = (TextView) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    TextView view = (TextView) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }
            }
        });

        butregis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    private void cekduplikasi() {
        String[] first = {"Y"};
        ArrayList<String> listemailsementara = new ArrayList<String>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Member").child(mCrypt.encrypt(edemail.getText().toString().trim())).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    listemailsementara.add(mCrypt.decrypt(String.valueOf(item.child("email").getValue())));

                }

                if (first[0].equals("Y")) {
                    String check = "N";
                    for (int i = 0; i < listemailsementara.size(); i++) {
                        if (listemailsementara.get(i).equals(edemail.getText().toString().trim())) {
                            check = "Y";
                        }
                    }
                    if (check.equals("Y")) {
                        pDialog.dismiss();
                        final Dialog mBottomSheetDialog;
                        final View view;
                        view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                        TextView judul = view.findViewById(R.id.judul);
                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                        final Button btnok = view.findViewById(R.id.Ya);
                        ImageView gambar = view.findViewById(R.id.gambar);
                        mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                        mBottomSheetDialog.setContentView(view);
                        mBottomSheetDialog.setCancelable(false);
                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                        mBottomSheetDialog.show();

                        judul.setText("Failed");
                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                        btnok.setText("Ok");

                        teks2.setText("Email Already Registered");

                        btnok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mBottomSheetDialog.dismiss();

                            }
                        });

                        btnok.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN: {
                                        Button view = (Button) v;
                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                        view.invalidate();
                                        break;
                                    }
                                    case MotionEvent.ACTION_UP:
                                    case MotionEvent.ACTION_CANCEL: {
                                        Button view = (Button) v;
                                        view.getBackground().clearColorFilter();
                                        view.invalidate();
                                        break;
                                    }
                                }

                                return false;
                            }
                        });
                    } else {

                        FirebaseDatabase db = FirebaseDatabase.getInstance();
                        DatabaseReference databaseReference1 = db.getReference("Member");

                        ModelMember mp = new ModelMember(mCrypt.encrypt(edusername.getText().toString().trim()),mCrypt.encrypt(edemail.getText().toString().trim()),mCrypt.encrypt(edpass.getText().toString().trim()),"","","");
                        databaseReference1.child(mCrypt.encrypt(edemail.getText().toString().trim())).push().setValue(mp).addOnSuccessListener(suc1 -> {
                            pDialog.dismiss();
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Success");
                            gambar.setBackground(getResources().getDrawable(R.drawable.benar));

                            btnok.setText("Ok");

                            teks2.setText("Account registration successful");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();
                                    Intent intent = new Intent(Register.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);



                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });


                        }).addOnFailureListener(er -> {
                            pDialog.dismiss();
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                            btnok.setText("Ok");

                            teks2.setText(er.getMessage()+"\nPlease Try Again");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();




                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });

                        });

                    }
                    hide();
                    first[0] = "N";
                } else {

                }


            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(Register.this).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(Register.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText(error+"\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();




                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    private void hide() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = this.getCurrentFocus();

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
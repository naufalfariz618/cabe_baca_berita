package id.baca.berita;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.ArrayList;
import java.util.Random;

import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.RESTAPI.SharedPrefUtils;
import id.baca.berita.model.ModelMember;

public class MainActivity extends AppCompatActivity {

    LinearLayout atas,bawah;
    boolean doubleBackToExitPressedOnce = false;
    TextView a1;
    EditText edemail;
    ShowHidePasswordEditText edpass;
    Button butlogin;
    MCrypt mCrypt;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    String username,email,foto,keynya,password;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.merah));
        }


        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z.]+";
        mCrypt = new MCrypt();
        username = "";
        email = "";
        foto = "";
        keynya = "";
        password = "";

        atas = findViewById(R.id.atas);
        bawah = findViewById(R.id.bawah);
        a1 = findViewById(R.id.a1);
        edemail = findViewById(R.id.edemail);
        edpass = findViewById(R.id.edpass);
        butlogin = findViewById(R.id.butlogin);

        atas.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide();
                return false;
            }
        });

        bawah.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide();
                return false;
            }
        });

        a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, Register.class);
                startActivity(a);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        a1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        TextView view = (TextView) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        TextView view = (TextView) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        butlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edemail.getText().toString().trim().equals("")||edemail.length()==0){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Email Still Empty");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (!edemail.getText().toString().trim().matches(emailPattern)){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Invalid Email");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (edpass.getText().toString().trim().equals("")||edpass.length()==0){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Password Still Empty");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else if (edpass.length()<7){
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Password must be 7 characters");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();



                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }else{
                    pDialog = new ProgressDialog(MainActivity.this);
                    pDialog.setCancelable(false);
                    pDialog.show();
                    pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                    pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    pDialog.setContentView(R.layout.layout_loader);
                    mFirebaseAuth = FirebaseAuth.getInstance();
                    mFirebaseUser = mFirebaseAuth.getCurrentUser();

                    mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                        cekdata();


                                    } else {
                                        final Dialog mBottomSheetDialog;
                                        final View view;
                                        view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                                        TextView judul = view.findViewById(R.id.judul);
                                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                                        final Button btnok = view.findViewById(R.id.Ya);
                                        ImageView gambar = view.findViewById(R.id.gambar);
                                        mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                                        mBottomSheetDialog.setContentView(view);
                                        mBottomSheetDialog.setCancelable(false);
                                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                        mBottomSheetDialog.show();

                                        judul.setText("Failed");
                                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                        btnok.setText("Ok");

                                        teks2.setText("Failed to load data, please try again");

                                        btnok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mBottomSheetDialog.dismiss();

                                            }
                                        });

                                        btnok.setOnTouchListener(new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                switch (event.getAction()) {
                                                    case MotionEvent.ACTION_DOWN: {
                                                        Button view = (Button) v;
                                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                        view.invalidate();
                                                        break;
                                                    }
                                                    case MotionEvent.ACTION_UP:
                                                    case MotionEvent.ACTION_CANCEL: {
                                                        Button view = (Button) v;
                                                        view.getBackground().clearColorFilter();
                                                        view.invalidate();
                                                        break;
                                                    }
                                                }

                                                return false;
                                            }
                                        });
                                        pDialog.dismiss();
                                    }
                                }
                            });
                }
            }
        });

        butlogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

    }

    private void cekdata() {
        String[] first = {"Y"};
        username = "";
        email = "";
        foto = "";
        keynya = "";
        password = "";
        ArrayList<String> listemailsementara = new ArrayList<String>();
        ArrayList<String> listpasssementara = new ArrayList<String>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Member").child(mCrypt.encrypt(edemail.getText().toString().trim())).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    listemailsementara.add(mCrypt.decrypt(String.valueOf(item.child("email").getValue())));
                    listpasssementara.add(mCrypt.decrypt(String.valueOf(item.child("password").getValue())));
                    if (mCrypt.decrypt(String.valueOf(item.child("email").getValue())).equals(edemail.getText().toString().trim())&&mCrypt.decrypt(String.valueOf(item.child("password").getValue())).equals(edpass.getText().toString().trim())){
                        email = mCrypt.decrypt(String.valueOf(item.child("email").getValue()));
                        keynya = item.getKey();
                        foto = String.valueOf(item.child("linkfoto").getValue());
                        password = mCrypt.decrypt(String.valueOf(item.child("password").getValue()));
                        username = mCrypt.decrypt(String.valueOf(item.child("username").getValue()));
                    }

                }

                if (first[0].equals("Y")) {
                    String check = "N";
                    for (int i = 0; i < listemailsementara.size(); i++) {
                        if (listemailsementara.get(i).equals(edemail.getText().toString().trim())) {
                            check = "Y";
                        }
                    }
                    if (check.equals("Y")) {
                        String checkp = "N";
                        for (int i = 0; i < listpasssementara.size(); i++) {
                            if (listpasssementara.get(i).equals(edpass.getText().toString().trim())) {
                                checkp = "Y";
                            }
                        }
                        if (checkp.equals("Y")){
                            String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                                    + "lmnopqrstuvwxyz!@#$%&-_";
                            Random rnd = new Random();
                            StringBuilder sb = new StringBuilder(100);
                            for (int i = 0; i < 100; i++) {
                                sb.append(chars.charAt(rnd.nextInt(chars.length())));
                            }

                            FirebaseDatabase db = FirebaseDatabase.getInstance();
                            DatabaseReference databaseReference1 = db.getReference("Member").child(mCrypt.encrypt(email)).child(keynya);

                            ModelMember mp = new ModelMember(mCrypt.encrypt(username),mCrypt.encrypt(email),mCrypt.encrypt(password),foto,keynya,String.valueOf(sb));
                            databaseReference1.setValue(mp).addOnSuccessListener(suc1 -> {
                                pDialog.dismiss();
                                SharedPrefUtils.setBooleanPreference(MainActivity.this, SharedPrefUtils.IS_LOGIN, true);
                                SharedPrefUtils.setStringPreference(MainActivity.this, SharedPrefUtils.USERNAME, username);
                                SharedPrefUtils.setStringPreference(MainActivity.this,SharedPrefUtils.EMAIL,email);
                                SharedPrefUtils.setStringPreference(MainActivity.this,SharedPrefUtils.FOTO,foto);
                                SharedPrefUtils.setStringPreference(MainActivity.this,SharedPrefUtils.SESI,String.valueOf(sb));
                                Intent intent = new Intent(MainActivity.this, Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


                            }).addOnFailureListener(er -> {
                                pDialog.dismiss();
                                final Dialog mBottomSheetDialog;
                                final View view;
                                view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                                TextView judul = view.findViewById(R.id.judul);
                                TextView teks2 = view.findViewById(R.id.pesanDialog);
                                final Button btnok = view.findViewById(R.id.Ya);
                                ImageView gambar = view.findViewById(R.id.gambar);
                                mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                                mBottomSheetDialog.setContentView(view);
                                mBottomSheetDialog.setCancelable(false);
                                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                mBottomSheetDialog.show();

                                judul.setText("Failed");
                                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                btnok.setText("Ok");

                                teks2.setText(er.getMessage()+"\nPlease Try Again");

                                btnok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mBottomSheetDialog.dismiss();

                                    }
                                });

                                btnok.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event) {
                                        switch (event.getAction()) {
                                            case MotionEvent.ACTION_DOWN: {
                                                Button view = (Button) v;
                                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                view.invalidate();
                                                break;
                                            }
                                            case MotionEvent.ACTION_UP:
                                            case MotionEvent.ACTION_CANCEL: {
                                                Button view = (Button) v;
                                                view.getBackground().clearColorFilter();
                                                view.invalidate();
                                                break;
                                            }
                                        }

                                        return false;
                                    }
                                });

                            });

                        }else{
                            pDialog.dismiss();
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                            btnok.setText("Ok");

                            teks2.setText("Wrong Passwrod");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();

                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });
                        }

                    } else {
                        pDialog.dismiss();
                        final Dialog mBottomSheetDialog;
                        final View view;
                        view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                        TextView judul = view.findViewById(R.id.judul);
                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                        final Button btnok = view.findViewById(R.id.Ya);
                        ImageView gambar = view.findViewById(R.id.gambar);
                        mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                        mBottomSheetDialog.setContentView(view);
                        mBottomSheetDialog.setCancelable(false);
                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                        mBottomSheetDialog.show();

                        judul.setText("Failed");
                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                        btnok.setText("Ok");

                        teks2.setText("Account Not Found");

                        btnok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mBottomSheetDialog.dismiss();

                            }
                        });

                        btnok.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN: {
                                        Button view = (Button) v;
                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                        view.invalidate();
                                        break;
                                    }
                                    case MotionEvent.ACTION_UP:
                                    case MotionEvent.ACTION_CANCEL: {
                                        Button view = (Button) v;
                                        view.getBackground().clearColorFilter();
                                        view.invalidate();
                                        break;
                                    }
                                }

                                return false;
                            }
                        });



                    }
                    hide();
                    first[0] = "N";
                } else {

                }


            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText(error+"\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();




                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    private void hide() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = this.getCurrentFocus();

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
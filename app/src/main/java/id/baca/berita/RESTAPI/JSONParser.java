package id.baca.berita.RESTAPI;

import android.os.Build;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import id.baca.berita.BuildConfig;


public class JSONParser {
    private static InputStream is = null;
    private static JSONObject jObj = null;
    private static String json = "";
    private X509TrustManager origTrustmanager;

    public JSONParser() {

    }

    public JSONObject makeHttpRequest(String url, String method, List<NameValuePair> params) throws JSONException {
        X509HostnameVerifier hostnameVerifier = new X509HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return BuildConfig.hostname.equals(hostname);
            }

            @Override
            public void verify(String s, SSLSocket sslSocket) throws IOException {

            }

            @Override
            public void verify(String s, X509Certificate x509Certificate) throws SSLException {

            }

            @Override
            public void verify(String s, String[] strings, String[] strings1) throws SSLException {

            }
        };

        KeyStore trustStore = null;
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            if (trustStore != null) {
                trustStore.load(null, null);
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        trustEveryone();

        org.apache.http.conn.ssl.SSLSocketFactory sf = null;
        try {
            sf = new MySSLSocketFactory(trustStore);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        if (sf != null) {
            sf.setHostnameVerifier(hostnameVerifier);
        }

        HttpParams params1 = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params1, 0);
        HttpConnectionParams.setSoTimeout(params1, 0);
        HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params1, HTTP.DEFAULT_CONTENT_CHARSET);
        HttpProtocolParams.setUseExpectContinue(params1, true);

        SchemeRegistry schemeRegistry = new SchemeRegistry();
        if (sf != null) {
            schemeRegistry.register(new Scheme("https", sf, 443));
        } else {
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        }
        ClientConnectionManager mgr = new SingleClientConnManager(params1, schemeRegistry);

        try {
            if (method.equals("POST")) {
                DefaultHttpClient httpClient = new DefaultHttpClient(mgr, params1);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } else if (method.equals("GET")) {
                DefaultHttpClient httpClient = new DefaultHttpClient(mgr, params1);
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1), 8);
            } else {
                reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            }
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            jObj = new JSONObject();
            jObj.put("success", "0");
            jObj.put("message", "Something went wrong !");
        }
        Log.i("tagconvertstr", "< "+json + ">");
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            jObj = new JSONObject();
            jObj.put("success", "0");
            jObj.put("message", "Something went wrong !");
        }
        return jObj;
    }

    private void trustEveryone() {
        try {
            TrustManagerFactory tmf = null;
            try {
                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                if (tmf != null) {
                    tmf.init((KeyStore) null);
                }
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
            TrustManager[] trustManagers = new TrustManager[0];
            if (tmf != null) {
                trustManagers = tmf.getTrustManagers();
            }
            this.origTrustmanager = (X509TrustManager) trustManagers[0];

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] certificates, String authType)
                        throws CertificateException {
                    if ((certificates != null) && (certificates.length == 1)) {
                        certificates[0].checkValidity();
                    } else {
                        origTrustmanager.checkClientTrusted(certificates, authType);
                    }
                }

                public void checkServerTrusted(X509Certificate[] certificates, String authType)
                        throws CertificateException {
                    if ((certificates != null) && (certificates.length == 1)) {
                        certificates[0].checkValidity();
                    } else {
                        origTrustmanager.checkServerTrusted(certificates, authType);
                    }
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }
            }}, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
package id.baca.berita;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.baca.berita.RESTAPI.SharedPrefUtils;

public class Splash extends AppCompatActivity {

    public static String x;
    int SPLASH_TIME_OUT = 2500;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        text = findViewById(R.id.text);

        Spannable wordtoSpan = new SpannableString(text.getText().toString().trim());

        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.biru)), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.merah)), 2, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.biru)), 8, 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setText(wordtoSpan);


        if (checkAndRequestPermissions()) {
            x = "";
            if (Build.VERSION.SDK_INT >= 23) {
                if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {
                    x = "sudah";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(Splash.this, Home.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();

                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    x = "belum";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, MainActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }
            } else {
                if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {


                    x = "sudah";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(Splash.this, Home.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();

                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    x = "belum";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, MainActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);

                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }
            }
        }

    }

    private boolean checkAndRequestPermissions() {
        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int write = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }

        if (write != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {
                    x = "sudah";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(Splash.this, Home.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();


                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    x = "belum";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, MainActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }
            } else {
                if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {


                    x = "sudah";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, Home.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                            finish();

                        }
                    }, SPLASH_TIME_OUT);
                } else {
                    x = "belum";
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, MainActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);

                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }
            }
        }
        return false;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0) {
                String sign;
                sign = "";
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.ACCESS_NETWORK_STATE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            sign = "Y";
                        } else {
                            sign = "N";
                            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Splash.this);
                            alertDialog2.setIcon(R.mipmap.ic_launcher_round);
                            alertDialog2.setTitle("Application Error");
                            alertDialog2.setMessage("Sorry...you have to give network and internet permissions so the application can run smoothly !");

                            alertDialog2.setCancelable(false);
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_NETWORK_STATE)) {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                checkAndRequestPermissions();
                                            }
                                        });
                            } else {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, 1);
                                                finish();
                                            }
                                        });
                            }
                            alertDialog2.show();
                            break;
                        }
                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            sign = "Y";
                        } else {
                            sign = "N";
                            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Splash.this);
                            alertDialog2.setIcon(R.mipmap.ic_launcher_round);
                            alertDialog2.setTitle("Application Error");
                            alertDialog2.setMessage("Sorry...you have to give storage access permission so the application can run smoothly !");

                            alertDialog2.setCancelable(false);
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                checkAndRequestPermissions();
                                            }
                                        });
                            } else {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, 1);
                                                finish();
                                            }
                                        });
                            }
                            alertDialog2.show();
                            break;
                        }
                    } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            sign = "Y";
                        } else {
                            sign = "N";
                            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Splash.this);
                            alertDialog2.setIcon(R.mipmap.ic_launcher_round);
                            alertDialog2.setTitle("Application Error");
                            alertDialog2.setMessage("Sorry...you have to give storage access permission so the application can run smoothly !");

                            alertDialog2.setCancelable(false);
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                checkAndRequestPermissions();
                                            }
                                        });
                            } else {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, 1);
                                                finish();
                                            }
                                        });
                            }
                            alertDialog2.show();
                            break;
                        }
                    } else if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            sign = "Y";
                        } else {
                            sign = "N";
                            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Splash.this);
                            alertDialog2.setIcon(R.mipmap.ic_launcher_round);
                            alertDialog2.setTitle("Application Error");
                            alertDialog2.setMessage("Sorry...you have to give camera access permission so the application can run smoothly !");

                            alertDialog2.setCancelable(false);
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                checkAndRequestPermissions();
                                            }
                                        });
                            } else {
                                alertDialog2.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, 1);
                                                finish();
                                            }
                                        });
                            }
                            alertDialog2.show();
                            break;
                        }
                    }
                }
                if (sign.equals("Y")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        x = "";
                        if (Build.VERSION.SDK_INT >= 23) {
                            if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {

                                x = "sudah";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, Home.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();


                                    }
                                }, SPLASH_TIME_OUT);
                            } else {
                                x = "belum";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, MainActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();
                                    }
                                }, SPLASH_TIME_OUT);
                            }
                        } else {
                            if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {


                                x = "sudah";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, Home.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();

                                    }
                                }, SPLASH_TIME_OUT);
                            } else {
                                x = "belum";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, MainActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);

                                        finish();
                                    }
                                }, SPLASH_TIME_OUT);
                            }
                        }

                    } else {
                        x = "";
                        if (Build.VERSION.SDK_INT >= 23) {
                            if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {
                                x = "sudah";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, Home.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();


                                    }
                                }, SPLASH_TIME_OUT);
                            } else {
                                x = "belum";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, MainActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();
                                    }
                                }, SPLASH_TIME_OUT);
                            }
                        } else {
                            if (SharedPrefUtils.getBooleanPreference(Splash.this, SharedPrefUtils.IS_LOGIN, false)) {


                                x = "sudah";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, Home.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                                        finish();

                                    }
                                }, SPLASH_TIME_OUT);
                            } else {
                                x = "belum";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(Splash.this, MainActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);

                                        finish();
                                    }
                                }, SPLASH_TIME_OUT);
                            }
                        }
                    }

                }
            }
        }
    }
}
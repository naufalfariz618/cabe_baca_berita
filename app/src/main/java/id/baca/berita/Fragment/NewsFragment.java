package id.baca.berita.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.baca.berita.BuildConfig;
import id.baca.berita.Home;
import id.baca.berita.MainActivity;
import id.baca.berita.R;
import id.baca.berita.RESTAPI.JSONParser;
import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.RESTAPI.SharedPrefUtils;
import id.baca.berita.subMenu.DetailBerita;

public class NewsFragment extends Fragment {

    public static final String TAG = "newsFragment";
    LinearLayout adadata, datanya;
    RelativeLayout nodata;
    String username, email, foto, sesi;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    MCrypt mCrypt;
    String success, pesan;
    ImageView gbrnodata;
    TextView txnodata,txnama;
    SwipeRefreshLayout swipe;
    NestedScrollView scroll;
    Animation animshow, animhide;
    CircleImageView ftprofil;
    ArrayList<String> listtanggal,listfoto,listlink,listpenulis,listorganisasi,listjudul,listdetail,listabstract,listid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        username = "";
        email = "";
        foto = "";
        sesi = "";
        success = "";
        pesan = "";
        mCrypt = new MCrypt();

        nodata = view.findViewById(R.id.nodata);
        nodata.setVisibility(View.VISIBLE);
        adadata = view.findViewById(R.id.adadata);
        adadata.setVisibility(View.GONE);
        datanya = view.findViewById(R.id.datanya);
        gbrnodata = view.findViewById(R.id.gbrnodata);
        txnodata = view.findViewById(R.id.txnodata);
        swipe = view.findViewById(R.id.swipe);
        scroll = view.findViewById(R.id.scroll);
        ftprofil = view.findViewById(R.id.ftprofile);
        txnama = view.findViewById(R.id.txnama);

        animhide = AnimationUtils.loadAnimation(requireActivity(), R.anim.popup_hide);
        animshow = AnimationUtils.loadAnimation(requireActivity(), R.anim.popup_show);

        pDialog = new ProgressDialog(requireActivity());
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.setContentView(R.layout.layout_loader);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();
                            showdata();


                        } else {
                            nodata.setVisibility(View.VISIBLE);
                            adadata.setVisibility(View.GONE);
                            gbrnodata.setBackground(getResources().getDrawable(R.drawable.noinet));
                            txnodata.setText("Failed to load data, please try again");
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                            btnok.setText("Ok");

                            teks2.setText("Failed to load data, please try again");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();

                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });

                            pDialog.dismiss();
                            swipe.setRefreshing(false);
                        }
                    }
                });
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                showdata();

            }
        });

        scroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                if (i3 < 0) {
                    Home.navbar.setVisibility(View.GONE);

                    Home.navbar.startAnimation(animhide);

                } else {
                    Home.navbar.setVisibility(View.VISIBLE);

                    Home.navbar.startAnimation(animshow);


                }
            }
        });

    }

    private void showdata() {
        if (isAdded()) {
            username = SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.USERNAME);
            email = SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.EMAIL);
            foto = SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO);
            sesi = SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.SESI);
        }
        String[] first = {"Y"};
        String[] pembanding = {""};
        ArrayList<String> listemailsementara = new ArrayList<String>();
        ArrayList<String> listpasssementara = new ArrayList<String>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Member").child(mCrypt.encrypt(email)).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    Log.e("s1", String.valueOf(item.child("sesi").getValue()));
                    Log.e("s2", sesi);
                    Log.e("username", username);
                    pembanding[0] = String.valueOf(item.child("sesi").getValue());
                    if (isAdded()) {
                        SharedPrefUtils.setStringPreference(requireActivity(),SharedPrefUtils.KEY,String.valueOf(item.child("keynya").getValue()));
                        SharedPrefUtils.setStringPreference(requireActivity(),SharedPrefUtils.FOTO,String.valueOf(item.child("linkfoto").getValue()));
                        SharedPrefUtils.setStringPreference(requireActivity(),SharedPrefUtils.USERNAME,mCrypt.decrypt(String.valueOf(item.child("username").getValue())));
                        SharedPrefUtils.setStringPreference(requireActivity(),SharedPrefUtils.EMAIL,mCrypt.decrypt(String.valueOf(item.child("email").getValue())));

                    }
                }

                if (first[0].equals("Y")) {
                    if (pembanding[0].equals(sesi)) {
                        lanjut();
                    } else {
                        pDialog.dismiss();
                        swipe.setRefreshing(false);
                        final Dialog mBottomSheetDialog;
                        final View view;
                        view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                        TextView judul = view.findViewById(R.id.judul);
                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                        final Button btnok = view.findViewById(R.id.Ya);
                        ImageView gambar = view.findViewById(R.id.gambar);
                        mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                        mBottomSheetDialog.setContentView(view);
                        mBottomSheetDialog.setCancelable(false);
                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                        mBottomSheetDialog.show();

                        judul.setText("Failed");
                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                        btnok.setText("Ok");

                        teks2.setText("Session Expired, Please Login Again");

                        btnok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mBottomSheetDialog.dismiss();
                                SharedPrefUtils.LogOut(requireActivity());
                                Intent intent = new Intent(requireActivity(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                            }
                        });

                        btnok.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN: {
                                        Button view = (Button) v;
                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                        view.invalidate();
                                        break;
                                    }
                                    case MotionEvent.ACTION_UP:
                                    case MotionEvent.ACTION_CANCEL: {
                                        Button view = (Button) v;
                                        view.getBackground().clearColorFilter();
                                        view.invalidate();
                                        break;
                                    }
                                }

                                return false;
                            }
                        });
                    }
                    first[0] = "N";
                } else {

                }


            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                swipe.setRefreshing(false);
                nodata.setVisibility(View.VISIBLE);
                adadata.setVisibility(View.GONE);
                gbrnodata.setBackground(getResources().getDrawable(R.drawable.noinet));
                txnodata.setText("Failed to load data, please try again");
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText(error + "\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();


                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });

    }

    private void lanjut() {
        new showdata().execute();
        if (isAdded()){
            if (SharedPrefUtils.getStringPreference(requireActivity(),SharedPrefUtils.FOTO).equals("")){
                ftprofil.setBackground(getResources().getDrawable(R.drawable.defaultfotoprofil));
            }else{
                ftprofil.setBackground(null);
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.defaultfotoprofil)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .error(R.drawable.defaultfotoprofil);

                Glide.with(requireActivity()).load(SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO)).apply(options).into(ftprofil);
            }
            txnama.setText(SharedPrefUtils.getStringPreference(requireActivity(),SharedPrefUtils.USERNAME));

        }



    }

    public class showdata extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("api-key",BuildConfig.apikey));


            JSONParser jParser = new JSONParser();

            JSONObject json = null;
            try {
                json = jParser.makeHttpRequest(BuildConfig.hostname, "GET", params);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                assert json != null;
                success = json.getString("status");
                listtanggal = new ArrayList<String>();
                listfoto = new ArrayList<String>();
                listlink = new ArrayList<String>();
                listpenulis = new ArrayList<String>();
                listorganisasi = new ArrayList<String>();
                listjudul = new ArrayList<String>();
                listdetail = new ArrayList<String>();
                listabstract = new ArrayList<String>();
                listid = new ArrayList<String>();
                if (success.equals("OK")){
                    JSONObject response = json.getJSONObject("response");
                    JSONArray docs = response.getJSONArray("docs");

                    for (int f = 0;f<docs.length();f++){
                        JSONObject jo = docs.getJSONObject(f);
//
                        listtanggal.add(jo.getString("pub_date"));
                        JSONArray multimedia = jo.getJSONArray("multimedia");
                        JSONObject mj = multimedia.getJSONObject(0);
                        listfoto.add(mj.getString("url"));
                        listlink.add(jo.getString("web_url"));
                        JSONObject byline = jo.getJSONObject("byline");
                        listpenulis.add(byline.getString("original"));
                        listorganisasi.add(byline.getString("organization"));
                        JSONObject headline = jo.getJSONObject("headline");
                        listjudul.add(headline.getString("main"));
                        listdetail.add(jo.getString("lead_paragraph"));
                        listabstract.add(jo.getString("abstract"));
                        listid.add(jo.getString("_id"));
//
                    }

                }



            } catch (Exception e) {
                // TODO: handle exception
                success = "0";
                pesan = "Something went wrong, Please Try Again";


            }

            return null;
        }

        @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            swipe.setRefreshing(false);

            if (success.equals("OK")) {
                if (listjudul.size()==0){
                    nodata.setVisibility(View.VISIBLE);
                    adadata.setVisibility(View.GONE);
                    gbrnodata.setBackground(getResources().getDrawable(R.drawable.nodata));
                    txnodata.setText("No Data Available");
                }else{
                    if (isAdded()){
                        createlist();

                    }
                }

            } else {
                nodata.setVisibility(View.VISIBLE);
                adadata.setVisibility(View.GONE);
                gbrnodata.setBackground(getResources().getDrawable(R.drawable.noinet));
                txnodata.setText(pesan);
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createlist() {
        nodata.setVisibility(View.GONE);
        adadata.setVisibility(View.VISIBLE);
        datanya.removeAllViewsInLayout();

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        int dp = (int) (getResources().getDimension(R.dimen.dimen_dp32) / getResources().getDisplayMetrics().density);

        p.setMargins(0, 0, 0, dp);


        int dp1 = (int) (getResources().getDimension(R.dimen.dimen_dp300) / getResources().getDisplayMetrics().density);

        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(
                dp1,
                dp1
        );
        p1.gravity = Gravity.CENTER;
        p1.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        p2.gravity = Gravity.CENTER_VERTICAL;
        int dp2 = (int) (getResources().getDimension(R.dimen.dimen_dp50) / getResources().getDisplayMetrics().density);
        p2.setMargins(dp2, 0, 0, 0);

        LinearLayout.LayoutParams teks1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        teks1.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams teks2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        teks2.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams butparam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        butparam.gravity = Gravity.RIGHT;
        butparam.setMargins(0, 0, 0, 0);

        for (int i = 0; i < listjudul.size(); i++) {
            LinearLayout layout2 = new LinearLayout(requireActivity());
            layout2.setLayoutParams(p);
            layout2.setOrientation(LinearLayout.HORIZONTAL);
            layout2.setBackground(getResources().getDrawable(R.drawable.kotak_putih));
            layout2.setElevation(10);
            layout2.setPadding(dp, dp, dp, dp);
            final int f = i;
            layout2.setId(f + 100000);
            layout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    klikdetail(f);
                }
            });
            layout2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            LinearLayout view = (LinearLayout) v;
                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                            view.invalidate();
                            break;
                        }
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL: {
                            LinearLayout view = (LinearLayout) v;
                            view.getBackground().clearColorFilter();
                            view.invalidate();
                            break;
                        }
                    }

                    return false;
                }
            });
            datanya.addView(layout2);

            ImageView ci = new ImageView(requireActivity());
            if (listfoto.get(i).equals("")) {
                ci.setBackground(getResources().getDrawable(R.drawable.nofoto));
            } else {
                ci.setBackground(null);
                Glide.with(requireActivity())
                        .load(BuildConfig.url_images + listfoto.get(i))
                        .placeholder(R.drawable.nofoto)
                        .error(R.drawable.nofoto)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(30)))
//                    .apply(RequestOptions.centerCropTransform().transform(new CenterCrop(), new GranularRoundedCorners(30, 30, 30, 30)))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).into(ci);
                ci.setScaleType(ImageView.ScaleType.FIT_XY);

            }
            ci.setLayoutParams(p1);
            layout2.addView(ci);

            LinearLayout layout3 = new LinearLayout(requireActivity());
            layout3.setLayoutParams(p2);
            layout3.setOrientation(LinearLayout.VERTICAL);
            layout2.addView(layout3);

            TextView txtanggal = new TextView(requireActivity());
            txtanggal.setLayoutParams(teks2);
            txtanggal.setText(listtanggal.get(i));
            txtanggal.setTextColor(getResources().getColor(R.color.hint));
            txtanggal.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.nr));
            int sp12 = (int) (getResources().getDimension(R.dimen.dimen_sp12) / getResources().getDisplayMetrics().density);
            txtanggal.setTextSize(sp12);
            txtanggal.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            layout3.addView(txtanggal);

            TextView txjudul = new TextView(requireActivity());
            txjudul.setLayoutParams(teks1);
            txjudul.setText(listjudul.get(i));
            txjudul.setMaxLines(1);
            txjudul.setEllipsize(TextUtils.TruncateAt.END);
            txjudul.setTextColor(getResources().getColor(R.color.black));
            txjudul.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.nb));
            int sp18 = (int) (getResources().getDimension(R.dimen.dimen_sp18) / getResources().getDisplayMetrics().density);
            txjudul.setTextSize(sp18);
            layout3.addView(txjudul);

            TextView txdetail = new TextView(requireActivity());
            txdetail.setLayoutParams(teks2);
            if (listabstract.get(i).equals("")||listabstract.get(i).equals("null")){
                txdetail.setText(listdetail.get(i));

            }else{
                txdetail.setText(listabstract.get(i));

            }
            txdetail.setMaxLines(2);
            txdetail.setEllipsize(TextUtils.TruncateAt.END);
            txdetail.setTextColor(getResources().getColor(R.color.black));
            txdetail.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.nr));
            int sp15 = (int) (getResources().getDimension(R.dimen.dimen_sp15) / getResources().getDisplayMetrics().density);
            txdetail.setTextSize(sp15);
            layout3.addView(txdetail);

            TextView txsumber = new TextView(requireActivity());
            txsumber.setLayoutParams(teks2);
            txsumber.setText(listpenulis.get(i));
            txsumber.setTextColor(getResources().getColor(R.color.kuning1));
            txsumber.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.nr));
            txsumber.setTextSize(sp15);
            layout3.addView(txsumber);
        }
    }

    private void klikdetail(int f) {
        LinearLayout layout2 = getView().findViewById(f+100000);

        Intent a = new Intent(requireActivity(), DetailBerita.class);
        a.putExtra("tanggal", listtanggal.get(f));
        a.putExtra("foto", listfoto.get(f));
        a.putExtra("link", listlink.get(f));
        a.putExtra("penulis", listpenulis.get(f));
        a.putExtra("organisasi", listorganisasi.get(f));
        a.putExtra("judul", listjudul.get(f));
        a.putExtra("detail", listdetail.get(f));
        a.putExtra("abstract", listabstract.get(f));
        a.putExtra("id", listid.get(f));
        startActivity(a);
        requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
package id.baca.berita.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import id.baca.berita.BuildConfig;
import id.baca.berita.Home;
import id.baca.berita.MainActivity;
import id.baca.berita.R;
import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.RESTAPI.SharedPrefUtils;
import id.baca.berita.Register;
import id.baca.berita.subMenu.Appinformation;
import id.baca.berita.subMenu.DetailBerita;
import id.baca.berita.subMenu.ProfileInformation;
import id.baca.berita.subMenu.SavedNews;

public class ProfileFragment extends Fragment {

    public static final String TAG = "profileFragment";
    NestedScrollView scroll;
    Animation animshow, animhide;
    CircleImageView ft, fotox;
    Button butlogout;
    String basefoto;
    Dialog mBottomSheetDialogpp;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    Bitmap thumbnail;
    String downloadUrl;
    MCrypt mCrypt;
    RelativeLayout reprof,resaved,reinfor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        animhide = AnimationUtils.loadAnimation(requireActivity(), R.anim.popup_hide);
        animshow = AnimationUtils.loadAnimation(requireActivity(), R.anim.popup_show);

        ft = view.findViewById(R.id.ft);
        butlogout = view.findViewById(R.id.butlogout);
        mCrypt = new MCrypt();
        reprof = view.findViewById(R.id.reprof);
        reinfor = view.findViewById(R.id.reinfo);
        resaved = view.findViewById(R.id.resaved);

        reprof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(requireActivity(), ProfileInformation.class);
                startActivity(a);
                requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        reprof.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        resaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(requireActivity(), SavedNews.class);
                startActivity(a);
                requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        resaved.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        reinfor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(requireActivity(), Appinformation.class);
                startActivity(a);
                requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        reinfor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        RelativeLayout view = (RelativeLayout) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });


        scroll = view.findViewById(R.id.scroll);
        scroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                if (i3 < 0) {
                    Home.navbar.setVisibility(View.GONE);

                    Home.navbar.startAnimation(animhide);

                } else {
                    Home.navbar.setVisibility(View.VISIBLE);

                    Home.navbar.startAnimation(animshow);


                }
            }
        });

        if (SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO).equals("")) {
            ft.setBackground(getResources().getDrawable(R.drawable.defaultfotoprofil));
        } else {
            ft.setBackground(null);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.defaultfotoprofil)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .error(R.drawable.defaultfotoprofil);

            Glide.with(requireActivity()).load(SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO)).apply(options).into(ft);

        }

        ft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basefoto = "";
                final View view;
                view = getLayoutInflater().inflate(R.layout.dialog_changefoto, null);
                TextView teks1 = view.findViewById(R.id.teks1);
                TextView btnok = view.findViewById(R.id.btnok);
                ImageButton btnclose = view.findViewById(R.id.btnclose);
                fotox = view.findViewById(R.id.foto);
                mBottomSheetDialogpp = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                mBottomSheetDialogpp.setContentView(view);
                mBottomSheetDialogpp.setCancelable(true);
                mBottomSheetDialogpp.setCanceledOnTouchOutside(true);
                mBottomSheetDialogpp.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialogpp.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialogpp.show();

                teks1.setText("Change Photo");
                btnok.setText("Save");

                if (!SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO).equals("")) {
                    fotox.setBackground(null);
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.defaultfotoprofil)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .error(R.drawable.defaultfotoprofil);

                    Glide.with(requireActivity()).load(SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.FOTO)).apply(options).into(fotox);
                } else {
                    fotox.setBackgroundResource(R.drawable.defaultfotoprofil);
                }
                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (basefoto.equals("")) {
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));


                            teks2.setText("You haven't selected a photo !");

                            btnok.setText("Ok");


                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();


                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });
                        } else {
                            pDialog = new ProgressDialog(requireActivity());
                            pDialog.setCancelable(false);
                            pDialog.show();
                            pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                            pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            pDialog.setContentView(R.layout.layout_loader);
                            mFirebaseAuth = FirebaseAuth.getInstance();
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();

                            mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                                    .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                                deletedulu();


                                            } else {
                                                final Dialog mBottomSheetDialog;
                                                final View view;
                                                view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                                                TextView judul = view.findViewById(R.id.judul);
                                                TextView teks2 = view.findViewById(R.id.pesanDialog);
                                                final Button btnok = view.findViewById(R.id.Ya);
                                                ImageView gambar = view.findViewById(R.id.gambar);
                                                mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                                                mBottomSheetDialog.setContentView(view);
                                                mBottomSheetDialog.setCancelable(false);
                                                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                                mBottomSheetDialog.show();

                                                judul.setText("Failed");
                                                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                                btnok.setText("Ok");

                                                teks2.setText("Failed to load data, please try again");

                                                btnok.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mBottomSheetDialog.dismiss();

                                                    }
                                                });

                                                btnok.setOnTouchListener(new View.OnTouchListener() {
                                                    @Override
                                                    public boolean onTouch(View v, MotionEvent event) {
                                                        switch (event.getAction()) {
                                                            case MotionEvent.ACTION_DOWN: {
                                                                Button view = (Button) v;
                                                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                                view.invalidate();
                                                                break;
                                                            }
                                                            case MotionEvent.ACTION_UP:
                                                            case MotionEvent.ACTION_CANCEL: {
                                                                Button view = (Button) v;
                                                                view.getBackground().clearColorFilter();
                                                                view.invalidate();
                                                                break;
                                                            }
                                                        }

                                                        return false;
                                                    }
                                                });

                                                pDialog.dismiss();
                                            }
                                        }
                                    });
                        }


                    }
                });
                fotox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final CharSequence[] items;

                        items = new CharSequence[]{"Pick From Camera", "Pick From Gallery", "Cancel"};

                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(requireActivity());

                        builder.setTitle("Change Profile Photo");

                        builder.setCancelable(true);
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (items[item].equals("Pick From Camera")) {
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, 11);
                                } else if (items[item].equals("Pick From Gallery")) {
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), 12);
                                } else if (items[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    }
                });

                btnclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialogpp.dismiss();
                    }
                });
            }
        });

        butlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog mBottomSheetDialog;
                final View view;
                view = getLayoutInflater().inflate(R.layout.dialog_konfirm, null);
                TextView btnok = view.findViewById(R.id.btnok);
                TextView btnno = view.findViewById(R.id.btnno);
                TextView txkeluar = view.findViewById(R.id.txkeluar);
                TextView txkeluar1 = view.findViewById(R.id.txkeluar1);
                mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();


                txkeluar.setText("Confirmation");
                txkeluar1.setText("Are you sure want to logout from this account?");
                btnok.setText("Yes");
                btnno.setText("No");


                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBottomSheetDialog.dismiss();
                        SharedPrefUtils.LogOut(requireActivity());
                        Intent intent = new Intent(requireActivity(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    }
                });


                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                TextView view = (TextView) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                TextView view = (TextView) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
                btnno.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();
                    }
                });

                btnno.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                TextView view = (TextView) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                TextView view = (TextView) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });

        butlogout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void deletedulu() {

        if (SharedPrefUtils.getStringPreference(requireActivity(),SharedPrefUtils.FOTO).equals("")){
            gantifoto();
        }else{
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
            StorageReference storageReference;
            storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(SharedPrefUtils.getStringPreference(requireActivity(),SharedPrefUtils.FOTO));
            storageReference.delete().addOnSuccessListener(suc ->{
                gantifoto();

            }).addOnFailureListener(er ->{
                pDialog.dismiss();
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText(er.getMessage()+"\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();



                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });

            });

        }

    }

    private void gantifoto() {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] finalimg = baos.toByteArray();
        final StorageReference filepath;
        filepath = storageReference.child("FotoProfile").child(finalimg + "jpg");
        final UploadTask uploadTask = filepath.putBytes(finalimg);
        uploadTask.addOnCompleteListener(getActivity(), new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @SuppressLint("ClickableViewAccessibility")
                                @Override
                                public void onSuccess(Uri uri) {
                                    downloadUrl = String.valueOf(uri);
                                    FirebaseDatabase db = FirebaseDatabase.getInstance();
                                    DatabaseReference databaseReference1 = db.getReference("Member").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(requireActivity(), SharedPrefUtils.EMAIL))).child(SharedPrefUtils.getStringPreference(requireActivity(),SharedPrefUtils.KEY));

                                    databaseReference1.child("linkfoto").setValue(downloadUrl).addOnSuccessListener(suc1 -> {
                                        pDialog.dismiss();
                                        mBottomSheetDialogpp.dismiss();
                                        final Dialog mBottomSheetDialog;
                                        final View view;
                                        view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                                        TextView judul = view.findViewById(R.id.judul);
                                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                                        final Button btnok = view.findViewById(R.id.Ya);
                                        ImageView gambar = view.findViewById(R.id.gambar);
                                        mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                                        mBottomSheetDialog.setContentView(view);
                                        mBottomSheetDialog.setCancelable(false);
                                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                        mBottomSheetDialog.show();

                                        judul.setText("Success");
                                        gambar.setBackground(getResources().getDrawable(R.drawable.benar));

                                        btnok.setText("Ok");

                                        teks2.setText("Change Photo Profile successful");


                                        btnok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mBottomSheetDialog.dismiss();
                                                Fragment fragment = new NewsFragment();
                                                requireActivity().getSupportFragmentManager()
                                                        .beginTransaction()
                                                        .replace(Home.frameLayout.getId(), fragment, "newsFragment")
                                                        .commit();
                                                Home.navbar.selectTabAt(0,true);
                                            }
                                        });

                                        btnok.setOnTouchListener(new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                switch (event.getAction()) {
                                                    case MotionEvent.ACTION_DOWN: {
                                                        Button view = (Button) v;
                                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                        view.invalidate();
                                                        break;
                                                    }
                                                    case MotionEvent.ACTION_UP:
                                                    case MotionEvent.ACTION_CANCEL: {
                                                        Button view = (Button) v;
                                                        view.getBackground().clearColorFilter();
                                                        view.invalidate();
                                                        break;
                                                    }
                                                }

                                                return false;
                                            }
                                        });

                                    }).addOnFailureListener(er -> {
                                        pDialog.dismiss();
                                        final Dialog mBottomSheetDialog;
                                        final View view;
                                        view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                                        TextView judul = view.findViewById(R.id.judul);
                                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                                        final Button btnok = view.findViewById(R.id.Ya);
                                        ImageView gambar = view.findViewById(R.id.gambar);
                                        mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                                        mBottomSheetDialog.setContentView(view);
                                        mBottomSheetDialog.setCancelable(false);
                                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                        mBottomSheetDialog.show();

                                        judul.setText("Failed");
                                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                        btnok.setText("Ok");

                                        teks2.setText(er.getMessage() + "\nPlease Try Again");

                                        btnok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mBottomSheetDialog.dismiss();

                                            }
                                        });

                                        btnok.setOnTouchListener(new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                switch (event.getAction()) {
                                                    case MotionEvent.ACTION_DOWN: {
                                                        Button view = (Button) v;
                                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                        view.invalidate();
                                                        break;
                                                    }
                                                    case MotionEvent.ACTION_UP:
                                                    case MotionEvent.ACTION_CANCEL: {
                                                        Button view = (Button) v;
                                                        view.getBackground().clearColorFilter();
                                                        view.invalidate();
                                                        break;
                                                    }
                                                }

                                                return false;
                                            }
                                        });

                                    });
                                }
                            });
                        }
                    });
                } else {
                    pDialog.dismiss();
                    final Dialog mBottomSheetDialog;
                    final View view;
                    view = LayoutInflater.from(requireActivity()).inflate(R.layout.dialog_salah, null);
                    TextView judul = view.findViewById(R.id.judul);
                    TextView teks2 = view.findViewById(R.id.pesanDialog);
                    final Button btnok = view.findViewById(R.id.Ya);
                    ImageView gambar = view.findViewById(R.id.gambar);
                    mBottomSheetDialog = new Dialog(requireActivity(), R.style.MaterialDialogSheet);
                    mBottomSheetDialog.setContentView(view);
                    mBottomSheetDialog.setCancelable(false);
                    mBottomSheetDialog.setCanceledOnTouchOutside(false);
                    mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                    mBottomSheetDialog.show();

                    judul.setText("Failed");
                    gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                    btnok.setText("Ok");

                    teks2.setText("Error\nPlease Try Again");

                    btnok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mBottomSheetDialog.dismiss();

                        }
                    });

                    btnok.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN: {
                                    Button view = (Button) v;
                                    view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                    view.invalidate();
                                    break;
                                }
                                case MotionEvent.ACTION_UP:
                                case MotionEvent.ACTION_CANCEL: {
                                    Button view = (Button) v;
                                    view.getBackground().clearColorFilter();
                                    view.invalidate();
                                    break;
                                }
                            }

                            return false;
                        }
                    });
                }
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 11) {
                onCaptureImageResult(data, requestCode);
            } else if (requestCode == 12) {
                onSelectFromGalleryResult(data, requestCode);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data, final int requestCode) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        thumbnail = bm;
        final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        if (requestCode == 12) {
            basefoto = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            fotox.setBackground(null);
            fotox.setImageBitmap(thumbnail);
        }
    }

    private void onCaptureImageResult(Intent data, final int requestCode) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.DIRECTORY_PICTURES, System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (requestCode == 11) {
            basefoto = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            fotox.setBackground(null);
            fotox.setImageBitmap(thumbnail);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
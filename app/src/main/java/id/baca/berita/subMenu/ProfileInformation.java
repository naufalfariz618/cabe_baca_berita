package id.baca.berita.subMenu;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import id.baca.berita.BuildConfig;
import id.baca.berita.R;
import id.baca.berita.RESTAPI.SharedPrefUtils;

public class ProfileInformation extends AppCompatActivity {

    ImageButton back;
    EditText edusername,edemail;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_information);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        back = findViewById(R.id.back);
        edusername = findViewById(R.id.edusername);
        edusername.setFocusable(false);
        edusername.setLongClickable(false);
        edusername.setCursorVisible(false);
        edemail = findViewById(R.id.edemail);
        edemail.setFocusable(false);
        edemail.setLongClickable(false);
        edemail.setCursorVisible(false);

        edusername.setText(SharedPrefUtils.getStringPreference(ProfileInformation.this,SharedPrefUtils.USERNAME));
        edemail.setText(SharedPrefUtils.getStringPreference(ProfileInformation.this,SharedPrefUtils.EMAIL));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
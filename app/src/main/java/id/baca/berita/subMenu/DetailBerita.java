package id.baca.berita.subMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import id.baca.berita.BuildConfig;
import id.baca.berita.MainActivity;
import id.baca.berita.R;
import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.RESTAPI.SharedPrefUtils;
import id.baca.berita.model.ModelBerita;

public class DetailBerita extends AppCompatActivity {
    ImageButton back;
    String tanggal,foto,link,penulis,organisasi,judul,detail,abstract1,id;
    ImageView fotodetail;
    ImageButton butsave;
    String tandasave;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    MCrypt mCrypt;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_berita);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        tanggal = "";
        foto = "";
        link = "";
        penulis = "";
        organisasi = "";
        judul = "";
        detail = "";
        abstract1 = "";
        id = "";
        tandasave = "N";
        mCrypt = new MCrypt();

        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {


            tanggal = bundle.getString("tanggal");
            foto = bundle.getString("foto");
            link = bundle.getString("link");
            penulis = bundle.getString("penulis");
            organisasi = bundle.getString("organisasi");
            judul = bundle.getString("judul");
            detail = bundle.getString("detail");
            abstract1 = bundle.getString("abstract");
            id = bundle.getString("id");

        } else {
            tanggal = "";
            foto = "";
            link = "";
            penulis = "";
            organisasi = "";
            judul = "";
            detail = "";
            abstract1 = "";
            id = "";
        }

        back = findViewById(R.id.back);
        fotodetail = findViewById(R.id.fotodetail);
        TextView txtanggal = findViewById(R.id.txtanggal);
        txtanggal.setText(tanggal);
        TextView txpenulis = findViewById(R.id.txpenulis);
        if (organisasi.equals("")||organisasi.equals("null")){
            txpenulis.setText(penulis);
        }else{
            txpenulis.setText(penulis+" "+organisasi);
        }
        TextView txjudul = findViewById(R.id.txjudul);
        txjudul.setText(judul);
        TextView txlink = findViewById(R.id.txlink);
        txlink.setText(link);
        TextView txdetail = findViewById(R.id.txdetail);
        txdetail.setText(detail);
        butsave = findViewById(R.id.butsave);

        txlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link));
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        txlink.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        TextView view = (TextView) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        TextView view = (TextView) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        pDialog = new ProgressDialog(DetailBerita.this);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.setContentView(R.layout.layout_loader);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                .addOnCompleteListener(DetailBerita.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();
                            cekid();


                        } else {
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                            btnok.setText("Ok");

                            teks2.setText("Error\nPlease Try Again");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();



                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });
                            pDialog.dismiss();
                        }
                    }
                });

        butsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tandasave.equals("Y")){
                    pDialog = new ProgressDialog(DetailBerita.this);
                    pDialog.setCancelable(false);
                    pDialog.show();
                    pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                    pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    pDialog.setContentView(R.layout.layout_loader);
                    mFirebaseAuth = FirebaseAuth.getInstance();
                    mFirebaseUser = mFirebaseAuth.getCurrentUser();

                    mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                            .addOnCompleteListener(DetailBerita.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                        hapusdata();


                                    } else {
                                        final Dialog mBottomSheetDialog;
                                        final View view;
                                        view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                                        TextView judul = view.findViewById(R.id.judul);
                                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                                        final Button btnok = view.findViewById(R.id.Ya);
                                        ImageView gambar = view.findViewById(R.id.gambar);
                                        mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                                        mBottomSheetDialog.setContentView(view);
                                        mBottomSheetDialog.setCancelable(false);
                                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                        mBottomSheetDialog.show();

                                        judul.setText("Failed");
                                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                        btnok.setText("Ok");

                                        teks2.setText("Error\nPlease Try Again");

                                        btnok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mBottomSheetDialog.dismiss();



                                            }
                                        });

                                        btnok.setOnTouchListener(new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                switch (event.getAction()) {
                                                    case MotionEvent.ACTION_DOWN: {
                                                        Button view = (Button) v;
                                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                        view.invalidate();
                                                        break;
                                                    }
                                                    case MotionEvent.ACTION_UP:
                                                    case MotionEvent.ACTION_CANCEL: {
                                                        Button view = (Button) v;
                                                        view.getBackground().clearColorFilter();
                                                        view.invalidate();
                                                        break;
                                                    }
                                                }

                                                return false;
                                            }
                                        });
                                        pDialog.dismiss();
                                    }
                                }
                            });
                }else{
                    pDialog = new ProgressDialog(DetailBerita.this);
                    pDialog.setCancelable(false);
                    pDialog.show();
                    pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                    pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    pDialog.setContentView(R.layout.layout_loader);
                    mFirebaseAuth = FirebaseAuth.getInstance();
                    mFirebaseUser = mFirebaseAuth.getCurrentUser();

                    mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                            .addOnCompleteListener(DetailBerita.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mFirebaseUser = mFirebaseAuth.getCurrentUser();
                                        tambahdata();


                                    } else {
                                        final Dialog mBottomSheetDialog;
                                        final View view;
                                        view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                                        TextView judul = view.findViewById(R.id.judul);
                                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                                        final Button btnok = view.findViewById(R.id.Ya);
                                        ImageView gambar = view.findViewById(R.id.gambar);
                                        mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                                        mBottomSheetDialog.setContentView(view);
                                        mBottomSheetDialog.setCancelable(false);
                                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                                        mBottomSheetDialog.show();

                                        judul.setText("Failed");
                                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                                        btnok.setText("Ok");

                                        teks2.setText("Error\nPlease Try Again");

                                        btnok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mBottomSheetDialog.dismiss();



                                            }
                                        });

                                        btnok.setOnTouchListener(new View.OnTouchListener() {
                                            @Override
                                            public boolean onTouch(View v, MotionEvent event) {
                                                switch (event.getAction()) {
                                                    case MotionEvent.ACTION_DOWN: {
                                                        Button view = (Button) v;
                                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                                        view.invalidate();
                                                        break;
                                                    }
                                                    case MotionEvent.ACTION_UP:
                                                    case MotionEvent.ACTION_CANCEL: {
                                                        Button view = (Button) v;
                                                        view.getBackground().clearColorFilter();
                                                        view.invalidate();
                                                        break;
                                                    }
                                                }

                                                return false;
                                            }
                                        });
                                        pDialog.dismiss();
                                    }
                                }
                            });
                }
            }
        });

        butsave.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        if (foto.equals("")){
            fotodetail.setBackground(getResources().getDrawable(R.drawable.nofoto));
        }else{
            fotodetail.setBackground(null);
            Glide.with(DetailBerita.this)
                    .load(BuildConfig.url_images + foto)
                    .placeholder(R.drawable.nofoto)
                    .error(R.drawable.nofoto)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .apply(RequestOptions.bitmapTransform(new RoundedCorners(0)))
//                    .apply(RequestOptions.centerCropTransform().transform(new CenterCrop(), new GranularRoundedCorners(30, 30, 30, 30)))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(fotodetail);
            fotodetail.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        fotodetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.zoom_foto, null);
                PhotoView gbr = view.findViewById(R.id.gbr);
                mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(true);
                mBottomSheetDialog.setCanceledOnTouchOutside(true);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                if (foto.equals("")){
                    gbr.setBackgroundResource(R.drawable.nofoto);
                }else {
                    gbr.setBackground(null);
                    Glide.with(DetailBerita.this)
                            .load(BuildConfig.url_images+foto)
                            .placeholder(R.drawable.nofoto)
                            .error(R.drawable.nofoto)
                            .skipMemoryCache(true)
                            .override(1000, 1000)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .apply(RequestOptions.fitCenterTransform().transform(new FitCenter(), new GranularRoundedCorners(0, 0, 0, 0)))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .into(gbr);
                    gbr.setScaleType(ImageView.ScaleType.FIT_CENTER);
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    private void cekid() {
        String[] tanda = {"Y"};
        String[] keynya = {""};
        String[] idnya = {""};
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Berita").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(DetailBerita.this,SharedPrefUtils.EMAIL))).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    if (mCrypt.decrypt(String.valueOf(item.child("id").getValue())).equals(id)){
                        keynya[0] = item.getKey();
                        idnya[0] = mCrypt.decrypt(String.valueOf(item.child("id").getValue()));
                        Log.e("idnya",idnya[0]);
                        Log.e("idqqqnya",id);
                    }

                }

                if (tanda[0].equals("Y")){
                    if (idnya[0].equals(id)){
                        butsave.setBackground(getResources().getDrawable(R.drawable.saveisi));
                        tandasave = "Y";
                    }else{
                        butsave.setBackground(getResources().getDrawable(R.drawable.savekosong));
                        tandasave = "N";
                    }
                    tanda[0] = "N";
                    pDialog.dismiss();
                }else{

                }

            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText("Error"+"\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();



                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    private void hapusdata() {
        String[] tanda = {"Y"};
        String[] keynya = {""};
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Berita").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(DetailBerita.this,SharedPrefUtils.EMAIL))).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
                    if (mCrypt.decrypt(String.valueOf(item.child("id").getValue())).equals(id)){
                        keynya[0] = item.getKey();
                    }

                }

                if (tanda[0].equals("Y")){
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                    databaseReference.child("Berita").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(DetailBerita.this,SharedPrefUtils.EMAIL))).child(keynya[0]).removeValue().addOnSuccessListener(suc -> {
                        pDialog.dismiss();
                        butsave.setBackground(getResources().getDrawable(R.drawable.savekosong));
                        tandasave = "N";

                    }).addOnFailureListener(er -> {
                        pDialog.dismiss();
                        final Dialog mBottomSheetDialog;
                        final View view;
                        view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                        TextView judul = view.findViewById(R.id.judul);
                        TextView teks2 = view.findViewById(R.id.pesanDialog);
                        final Button btnok = view.findViewById(R.id.Ya);
                        ImageView gambar = view.findViewById(R.id.gambar);
                        mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                        mBottomSheetDialog.setContentView(view);
                        mBottomSheetDialog.setCancelable(false);
                        mBottomSheetDialog.setCanceledOnTouchOutside(false);
                        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                        mBottomSheetDialog.show();

                        judul.setText("Failed");
                        gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                        btnok.setText("Ok");

                        teks2.setText(er.getMessage()+"\nPlease Try Again");

                        btnok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mBottomSheetDialog.dismiss();



                            }
                        });

                        btnok.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN: {
                                        Button view = (Button) v;
                                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                        view.invalidate();
                                        break;
                                    }
                                    case MotionEvent.ACTION_UP:
                                    case MotionEvent.ACTION_CANCEL: {
                                        Button view = (Button) v;
                                        view.getBackground().clearColorFilter();
                                        view.invalidate();
                                        break;
                                    }
                                }

                                return false;
                            }
                        });

                    });
                    tanda[0] = "N";
                }else{

                }

            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText("Error"+"\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();



                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void tambahdata() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference1 = db.getReference("Berita").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(DetailBerita.this,SharedPrefUtils.EMAIL)));

        ModelBerita mp = new ModelBerita(mCrypt.encrypt(tanggal), mCrypt.encrypt(foto), mCrypt.encrypt(link), mCrypt.encrypt(penulis), "",mCrypt.encrypt(organisasi),mCrypt.encrypt(judul),mCrypt.encrypt(detail),mCrypt.encrypt(abstract1),mCrypt.encrypt(id));
        databaseReference1.push().setValue(mp).addOnSuccessListener(suc1 -> {

            pDialog.dismiss();
            butsave.setBackground(getResources().getDrawable(R.drawable.saveisi));
            tandasave = "Y";

        }).addOnFailureListener(er -> {
            pDialog.dismiss();

            final Dialog mBottomSheetDialog;
            final View view;
            view = LayoutInflater.from(DetailBerita.this).inflate(R.layout.dialog_salah, null);
            TextView judul = view.findViewById(R.id.judul);
            TextView teks2 = view.findViewById(R.id.pesanDialog);
            final Button btnok = view.findViewById(R.id.Ya);
            ImageView gambar = view.findViewById(R.id.gambar);
            mBottomSheetDialog = new Dialog(DetailBerita.this, R.style.MaterialDialogSheet);
            mBottomSheetDialog.setContentView(view);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
            mBottomSheetDialog.show();

            judul.setText("Failed");
            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

            btnok.setText("Ok");

            teks2.setText(er.getMessage()+"\nPlease Try Again");

            btnok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();



                }
            });

            btnok.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            Button view = (Button) v;
                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                            view.invalidate();
                            break;
                        }
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL: {
                            Button view = (Button) v;
                            view.getBackground().clearColorFilter();
                            view.invalidate();
                            break;
                        }
                    }

                    return false;
                }
            });

        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
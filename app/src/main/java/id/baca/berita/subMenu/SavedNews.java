package id.baca.berita.subMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import id.baca.berita.BuildConfig;
import id.baca.berita.MainActivity;
import id.baca.berita.R;
import id.baca.berita.RESTAPI.MCrypt;
import id.baca.berita.RESTAPI.SharedPrefUtils;

public class SavedNews extends AppCompatActivity {

    ImageButton back;
    LinearLayout adadata, datanya;
    RelativeLayout nodata;
    TextView txnodata;
    ImageView gbrnodata;
    SwipeRefreshLayout swipe;
    ProgressDialog pDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    MCrypt mCrypt;
    ArrayList<String> listtanggal, listfoto, listlink, listpenulis, listorganisasi, listjudul, listdetail, listabstract, listid;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_news);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        back = findViewById(R.id.back);
        nodata = findViewById(R.id.nodata);
        nodata.setVisibility(View.VISIBLE);
        adadata = findViewById(R.id.adadata);
        adadata.setVisibility(View.GONE);
        datanya = findViewById(R.id.datanya);
        gbrnodata = findViewById(R.id.gbrnodata);
        txnodata = findViewById(R.id.txnodata);
        swipe = findViewById(R.id.swipe);
        mCrypt = new MCrypt();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });

        pDialog = new ProgressDialog(SavedNews.this);
        pDialog.setCancelable(false);
        pDialog.show();
        pDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        pDialog.getWindow().setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.setContentView(R.layout.layout_loader);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        mFirebaseAuth.signInWithEmailAndPassword("naufalfariz618@gmail.com", BuildConfig.kode)
                .addOnCompleteListener(SavedNews.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();
                            showdata();


                        } else {
                            nodata.setVisibility(View.VISIBLE);
                            adadata.setVisibility(View.GONE);
                            gbrnodata.setBackground(getResources().getDrawable(R.drawable.noinet));
                            txnodata.setText("Failed to load data, please try again");
                            final Dialog mBottomSheetDialog;
                            final View view;
                            view = LayoutInflater.from(SavedNews.this).inflate(R.layout.dialog_salah, null);
                            TextView judul = view.findViewById(R.id.judul);
                            TextView teks2 = view.findViewById(R.id.pesanDialog);
                            final Button btnok = view.findViewById(R.id.Ya);
                            ImageView gambar = view.findViewById(R.id.gambar);
                            mBottomSheetDialog = new Dialog(SavedNews.this, R.style.MaterialDialogSheet);
                            mBottomSheetDialog.setContentView(view);
                            mBottomSheetDialog.setCancelable(false);
                            mBottomSheetDialog.setCanceledOnTouchOutside(false);
                            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                            mBottomSheetDialog.show();

                            judul.setText("Failed");
                            gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                            btnok.setText("Ok");

                            teks2.setText("Failed to load data, please try again");

                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBottomSheetDialog.dismiss();

                                }
                            });

                            btnok.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case MotionEvent.ACTION_DOWN: {
                                            Button view = (Button) v;
                                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                            view.invalidate();
                                            break;
                                        }
                                        case MotionEvent.ACTION_UP:
                                        case MotionEvent.ACTION_CANCEL: {
                                            Button view = (Button) v;
                                            view.getBackground().clearColorFilter();
                                            view.invalidate();
                                            break;
                                        }
                                    }

                                    return false;
                                }
                            });

                            pDialog.dismiss();
                            swipe.setRefreshing(false);
                        }
                    }
                });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                showdata();

            }
        });
    }

    private void showdata() {
        String[] tanda = {"Y"};
        listtanggal = new ArrayList<String>();
        listfoto = new ArrayList<String>();
        listlink = new ArrayList<String>();
        listpenulis = new ArrayList<String>();
        listorganisasi = new ArrayList<String>();
        listjudul = new ArrayList<String>();
        listdetail = new ArrayList<String>();
        listabstract = new ArrayList<String>();
        listid = new ArrayList<String>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Berita").child(mCrypt.encrypt(SharedPrefUtils.getStringPreference(SavedNews.this, SharedPrefUtils.EMAIL))).addValueEventListener(new ValueEventListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot item : snapshot.getChildren()) {
//                    if (mCrypt.decrypt(String.valueOf(item.child("id").getValue())).equals(id)){
//                        keynya[0] = item.getKey();
//                        idnya[0] = mCrypt.decrypt(String.valueOf(item.child("id").getValue()));
//                        Log.e("idnya",idnya[0]);
//                        Log.e("idqqqnya",id);
//                    }

                    listtanggal.add(mCrypt.decrypt(String.valueOf(item.child("tanggal").getValue())));
                    listfoto.add(mCrypt.decrypt(String.valueOf(item.child("foto").getValue())));
                    listlink.add(mCrypt.decrypt(String.valueOf(item.child("link").getValue())));
                    listpenulis.add(mCrypt.decrypt(String.valueOf(item.child("penulis").getValue())));
                    listorganisasi.add(mCrypt.decrypt(String.valueOf(item.child("organisasi").getValue())));
                    listjudul.add(mCrypt.decrypt(String.valueOf(item.child("judul").getValue())));
                    listdetail.add(mCrypt.decrypt(String.valueOf(item.child("detail").getValue())));
                    listabstract.add(mCrypt.decrypt(String.valueOf(item.child("abstract1").getValue())));
                    listid.add(mCrypt.decrypt(String.valueOf(item.child("id").getValue())));

                }

                if (tanda[0].equals("Y")) {
                    if (listjudul.size() == 0) {
                        nodata.setVisibility(View.VISIBLE);
                        adadata.setVisibility(View.GONE);
                        gbrnodata.setBackground(getResources().getDrawable(R.drawable.nodata));
                        txnodata.setText("No Data Available");
                    } else {

                        createlist();


                    }
                    tanda[0] = "N";
                    pDialog.dismiss();
                    swipe.setRefreshing(false);
                } else {

                }

            }


            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                pDialog.dismiss();
                swipe.setRefreshing(false);
                final Dialog mBottomSheetDialog;
                final View view;
                view = LayoutInflater.from(SavedNews.this).inflate(R.layout.dialog_salah, null);
                TextView judul = view.findViewById(R.id.judul);
                TextView teks2 = view.findViewById(R.id.pesanDialog);
                final Button btnok = view.findViewById(R.id.Ya);
                ImageView gambar = view.findViewById(R.id.gambar);
                mBottomSheetDialog = new Dialog(SavedNews.this, R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(view);
                mBottomSheetDialog.setCancelable(false);
                mBottomSheetDialog.setCanceledOnTouchOutside(false);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);
                mBottomSheetDialog.show();

                judul.setText("Failed");
                gambar.setBackground(getResources().getDrawable(R.drawable.salah));

                btnok.setText("Ok");

                teks2.setText("Error" + "\nPlease Try Again");

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.dismiss();


                    }
                });

                btnok.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                Button view = (Button) v;
                                view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                                view.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP:
                            case MotionEvent.ACTION_CANCEL: {
                                Button view = (Button) v;
                                view.getBackground().clearColorFilter();
                                view.invalidate();
                                break;
                            }
                        }

                        return false;
                    }
                });
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createlist() {

        nodata.setVisibility(View.GONE);
        adadata.setVisibility(View.VISIBLE);
        datanya.removeAllViewsInLayout();

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        int dp = (int) (getResources().getDimension(R.dimen.dimen_dp32) / getResources().getDisplayMetrics().density);

        p.setMargins(0, 0, 0, dp);


        int dp1 = (int) (getResources().getDimension(R.dimen.dimen_dp300) / getResources().getDisplayMetrics().density);

        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(
                dp1,
                dp1
        );
        p1.gravity = Gravity.CENTER;
        p1.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        p2.gravity = Gravity.CENTER_VERTICAL;
        int dp2 = (int) (getResources().getDimension(R.dimen.dimen_dp50) / getResources().getDisplayMetrics().density);
        p2.setMargins(dp2, 0, 0, 0);

        LinearLayout.LayoutParams teks1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        teks1.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams teks2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        teks2.setMargins(0, 0, 0, 0);

        LinearLayout.LayoutParams butparam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        butparam.gravity = Gravity.RIGHT;
        butparam.setMargins(0, 0, 0, 0);

        for (int i = 0; i < listjudul.size(); i++) {
            LinearLayout layout2 = new LinearLayout(SavedNews.this);
            layout2.setLayoutParams(p);
            layout2.setOrientation(LinearLayout.HORIZONTAL);
            layout2.setBackground(getResources().getDrawable(R.drawable.kotak_putih));
            layout2.setElevation(10);
            layout2.setPadding(dp, dp, dp, dp);
            final int f = i;
            layout2.setId(f + 100000);
            layout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    klikdetail(f);
                }
            });
            layout2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            LinearLayout view = (LinearLayout) v;
                            view.getBackground().setColorFilter(getResources().getColor(R.color.abu), PorterDuff.Mode.SRC_ATOP);
                            view.invalidate();
                            break;
                        }
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL: {
                            LinearLayout view = (LinearLayout) v;
                            view.getBackground().clearColorFilter();
                            view.invalidate();
                            break;
                        }
                    }

                    return false;
                }
            });
            datanya.addView(layout2);

            ImageView ci = new ImageView(SavedNews.this);
            if (listfoto.get(i).equals("")) {
                ci.setBackground(getResources().getDrawable(R.drawable.nofoto));
            } else {
                ci.setBackground(null);
                Glide.with(SavedNews.this)
                        .load(BuildConfig.url_images + listfoto.get(i))
                        .placeholder(R.drawable.nofoto)
                        .error(R.drawable.nofoto)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(30)))
//                    .apply(RequestOptions.centerCropTransform().transform(new CenterCrop(), new GranularRoundedCorners(30, 30, 30, 30)))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).into(ci);
                ci.setScaleType(ImageView.ScaleType.FIT_XY);

            }
            ci.setLayoutParams(p1);
            layout2.addView(ci);

            LinearLayout layout3 = new LinearLayout(SavedNews.this);
            layout3.setLayoutParams(p2);
            layout3.setOrientation(LinearLayout.VERTICAL);
            layout2.addView(layout3);

            TextView txtanggal = new TextView(SavedNews.this);
            txtanggal.setLayoutParams(teks2);
            txtanggal.setText(listtanggal.get(i));
            txtanggal.setTextColor(getResources().getColor(R.color.hint));
            txtanggal.setTypeface(ResourcesCompat.getFont(SavedNews.this, R.font.nr));
            int sp12 = (int) (getResources().getDimension(R.dimen.dimen_sp12) / getResources().getDisplayMetrics().density);
            txtanggal.setTextSize(sp12);
            txtanggal.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            layout3.addView(txtanggal);

            TextView txjudul = new TextView(SavedNews.this);
            txjudul.setLayoutParams(teks1);
            txjudul.setText(listjudul.get(i));
            txjudul.setMaxLines(1);
            txjudul.setEllipsize(TextUtils.TruncateAt.END);
            txjudul.setTextColor(getResources().getColor(R.color.black));
            txjudul.setTypeface(ResourcesCompat.getFont(SavedNews.this, R.font.nb));
            int sp18 = (int) (getResources().getDimension(R.dimen.dimen_sp18) / getResources().getDisplayMetrics().density);
            txjudul.setTextSize(sp18);
            layout3.addView(txjudul);

            TextView txdetail = new TextView(SavedNews.this);
            txdetail.setLayoutParams(teks2);
            if (listabstract.get(i).equals("")||listabstract.get(i).equals("null")){
                txdetail.setText(listdetail.get(i));

            }else{
                txdetail.setText(listabstract.get(i));

            }
            txdetail.setMaxLines(2);
            txdetail.setEllipsize(TextUtils.TruncateAt.END);
            txdetail.setTextColor(getResources().getColor(R.color.black));
            txdetail.setTypeface(ResourcesCompat.getFont(SavedNews.this, R.font.nr));
            int sp15 = (int) (getResources().getDimension(R.dimen.dimen_sp15) / getResources().getDisplayMetrics().density);
            txdetail.setTextSize(sp15);
            layout3.addView(txdetail);

            TextView txsumber = new TextView(SavedNews.this);
            txsumber.setLayoutParams(teks2);
            txsumber.setText(listpenulis.get(i));
            txsumber.setTextColor(getResources().getColor(R.color.kuning1));
            txsumber.setTypeface(ResourcesCompat.getFont(SavedNews.this, R.font.nr));
            txsumber.setTextSize(sp15);
            layout3.addView(txsumber);
        }
    }

    private void klikdetail(int f) {
        LinearLayout layout2 = findViewById(f+100000);
        int LAUNCH_SECOND_ACTIVITY = 876;
        Intent a = new Intent(SavedNews.this, DetailBeritaSaved.class);
        a.putExtra("tanggal", listtanggal.get(f));
        a.putExtra("foto", listfoto.get(f));
        a.putExtra("link", listlink.get(f));
        a.putExtra("penulis", listpenulis.get(f));
        a.putExtra("organisasi", listorganisasi.get(f));
        a.putExtra("judul", listjudul.get(f));
        a.putExtra("detail", listdetail.get(f));
        a.putExtra("abstract", listabstract.get(f));
        a.putExtra("id", listid.get(f));
        startActivityForResult(a, LAUNCH_SECOND_ACTIVITY);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data1) {
        super.onActivityResult(requestCode, resultCode, data1);

        if (requestCode == 876) {
            if (resultCode == Activity.RESULT_OK) {
                showdata();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    }
}
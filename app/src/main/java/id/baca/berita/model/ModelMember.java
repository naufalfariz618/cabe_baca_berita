package id.baca.berita.model;

public class ModelMember {
    private String username;
    private String email;
    private String password;
    private String linkfoto;
    private String keynya;
    private String sesi;

    public ModelMember(){}

    public ModelMember(String username, String email, String password, String linkfoto, String keynya, String sesi) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.linkfoto = linkfoto;
        this.keynya = keynya;
        this.sesi = sesi;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLinkfoto() {
        return linkfoto;
    }

    public void setLinkfoto(String linkfoto) {
        this.linkfoto = linkfoto;
    }

    public String getKeynya() {
        return keynya;
    }

    public void setKeynya(String keynya) {
        this.keynya = keynya;
    }

    public String getSesi() {
        return sesi;
    }

    public void setSesi(String sesi) {
        this.sesi = sesi;
    }
}
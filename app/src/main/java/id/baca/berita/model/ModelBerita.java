package id.baca.berita.model;

public class ModelBerita {
    private String tanggal;
    private String foto;
    private String link;
    private String penulis;
    private String keynya;
    private String organisasi;
    private String judul;
    private String detail;
    private String abstract1;
    private String id;

    public ModelBerita(){}

    public ModelBerita(String tanggal, String foto, String link, String penulis, String keynya, String organisasi, String judul, String detail, String abstract1, String id) {
        this.tanggal = tanggal;
        this.foto = foto;
        this.link = link;
        this.penulis = penulis;
        this.keynya = keynya;
        this.organisasi = organisasi;
        this.judul = judul;
        this.detail = detail;
        this.abstract1 = abstract1;
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getKeynya() {
        return keynya;
    }

    public void setKeynya(String keynya) {
        this.keynya = keynya;
    }

    public String getOrganisasi() {
        return organisasi;
    }

    public void setOrganisasi(String organisasi) {
        this.organisasi = organisasi;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAbstract1() {
        return abstract1;
    }

    public void setAbstract1(String abstract1) {
        this.abstract1 = abstract1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
package id.baca.berita;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import id.baca.berita.Fragment.NewsFragment;
import id.baca.berita.Fragment.ProfileFragment;
import nl.joery.animatedbottombar.AnimatedBottomBar;

public class Home extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;

    public static AnimatedBottomBar navbar;

    public static FrameLayout frameLayout;

    private final AnimatedBottomBar.OnTabInterceptListener mOnNavigationListener
            = new AnimatedBottomBar.OnTabInterceptListener() {


        @Override
        public boolean onTabIntercepted(int i, @Nullable AnimatedBottomBar.Tab tab, int i1, @NonNull AnimatedBottomBar.Tab tab1) {
            switch (tab1.getId()) {
                case R.id.news:
                    showFragment(NewsFragment.TAG);

                    return true;
                case R.id.profile:
                    showFragment(ProfileFragment.TAG);


                    return true;



            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.biru));
        }

        hide();

        navbar = findViewById(R.id.bottom_bar);
        navbar.setOnTabInterceptListener(mOnNavigationListener);
        frameLayout = findViewById(R.id.fragment_layout);

        if (savedInstanceState == null) {
            showFragment(NewsFragment.TAG);
        }
    }

    private void showFragment(@NonNull String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            switch (tag) {
                case NewsFragment.TAG: {
                    fragment = new NewsFragment();
                    break;
                }
                case ProfileFragment.TAG: {
                    fragment = new ProfileFragment();
                    break;
                }
                default: {
                    fragment = new NewsFragment();
                    break;
                }
            }
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(frameLayout.getId(), fragment, tag)
                .commit();
    }

    private void hide() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = this.getCurrentFocus();

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}